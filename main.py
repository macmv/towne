import asyncio
import time
import os
import json
import subprocess
import platform

import discord as discord
from discord_slash import SlashCommand
from discord import FFmpegPCMAudio
from discord_slash.utils.manage_commands import create_option

client = discord.Client()
slash = SlashCommand(client, sync_commands=True)
token_file = open("token", "r")
token = token_file.read()
token_file.close()
guild_ids = [349402004549795840, 803542061977960448]  # Put your server ID in this array.
max_cache_size = 1000000000
max_video_size = 10000000
que = []


def creation_date(path_to_file):
    """
    Try to get the date that a file was created, falling back to when it was
    last modified if that isn't possible.
    See http://stackoverflow.com/a/39501288/1709587 for explanation.
    """
    if platform.system() == 'Windows':
        return os.path.getctime(path_to_file)
    else:
        stat = os.stat(path_to_file)
        try:
            return stat.st_birthtime
        except AttributeError:
            # We're probably on Linux. No easy way to get creation dates here,
            # so we'll settle for when its content was last modified.
            return stat.st_mtime


def get_size(start_path='.'):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            # skip if it is symbolic link
            if not os.path.islink(fp):
                total_size += os.path.getsize(fp)

    return total_size


@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))
    if not os.path.isdir("output-cache"):
        os.mkdir("output-cache")


@slash.slash(name="ping", description="Get ping time of the bot", guild_ids=guild_ids)
async def _ping(ctx):  # Defines a new "context" (ctx) command called "ping."
    await ctx.send(f"Pong! ({client.latency * 1000}ms)")


@slash.slash(name="play",
             description="Plays audio",
             options=[
                 create_option(
                     name="url",
                     option_type=3,
                     description="The url of the audio to play",
                     required=True
                 ),
                 create_option(
                     name="channel",
                     option_type=7,
                     description="The channel to play in",
                     required=False
                 )
             ],
             guild_ids=guild_ids)
async def _play(ctx, url, **kwargs):
    print("Cache size is " + str(get_size("output-cache")) + " bytes.")
    while get_size("output-cache") > max_cache_size:
        files = os.listdir("output-cache")
        oldest = files[0]
        for file in files:
            if creation_date("output-cache/" + oldest) < creation_date("output-cache/" + file):
                oldest = file
        os.remove("output-cache/" + oldest)
        print("Cache has been reduced to " + str(get_size("output-cache")) + " bytes.")
    ffmpeg_options = {'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5', 'options': '-vn'}
    video_json_data = subprocess.run(["youtube-dl", "--format", "249", url, "--print-json"], capture_output=True).stdout
    try:
        video_data = json.loads(video_json_data)
    except:
        await ctx.send("Could not find that video!")
        return
    if video_data["filesize"] > max_video_size:
        await ctx.send("Video is too big to play!")
        return
    v_client: discord.VoiceClient = discord.utils.get(ctx.bot.voice_clients, guild=ctx.guild)
    if v_client is not None and v_client.is_playing():
        v_client.stop()
    if not video_data["id"] + ".mp3" in os.listdir("output-cache"):
        await ctx.send("Playing...\n(This video is not cached so it will take a bit to play this time!)")
        subprocess.run(["youtube-dl", "-f", "249", "--audio-format", "mp3", "-x", url, "-o",
                        "output-cache/" + video_data["id"] + ".%(ext)s"])
    else:
        await ctx.send("Playing...")
    audio_source = FFmpegPCMAudio("output-cache/" + video_data["id"] + ".mp3", options=ffmpeg_options)
    files = os.listdir("./")
    for file in files:
        if file[-5:] == ".webm":
            os.remove(file)
    channel = None
    if "channel" in kwargs:
        channel = kwargs["channel"]
    elif ctx.author.voice is not None:
        channel = ctx.author.voice.channel
    if channel is None:
        await ctx.send("Please join a voice channel, or specify one with the channel argument!")
        return
    if v_client is None:
        v_client = await channel.connect()
    v_client.play(audio_source, after=None)


@slash.slash(name="stop", description="Disconnects the bot", guild_ids=guild_ids)
async def _stop(ctx):
    v_client: discord.VoiceClient = discord.utils.get(ctx.bot.voice_clients, guild=ctx.guild)
    if v_client is not None:
        await v_client.disconnect()
        await ctx.send("Stopped!")
        return
    await ctx.send("Nothing to stop!")


@slash.slash(name="playsean",
             description="Has Sean play audio",
             options=[
                 create_option(
                     name="url",
                     option_type=3,
                     description="The url of the audio to play",
                     required=True
                 ),
                 create_option(
                     name="channel",
                     option_type=7,
                     description="The channel to play in",
                     required=True
                 )
             ],
             guild_ids=guild_ids)
async def _play_sean(ctx, url, channel):
    sean = await client.fetch_user(262818343654785024)
    await sean.send("You have been requested to play " + url + " in the channel <#" + str(channel.id) + ">")
    await ctx.send("Requesting Sean...")


@slash.slash(name="stopsean", description="Stops Sean from playing", guild_ids=guild_ids)
async def _stop_sean(ctx):
    sean = await client.fetch_user(262818343654785024)
    await sean.send("You have been stopped")
    await ctx.send("Stopping Sean...")


client.run(token)
